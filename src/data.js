import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './Store/Action'

class Data extends Component {

    constructor() {
        super()
        console.log(`constructor`)
    }

    componentDidMount() {
        console.log(` cdm`)
        this.props.getUser()
    }

    render() {

        let userList = this.props.fetch.map(d => {
            console.log(d)
            return <ul key={d.id}>
                <li>{d.name}</li>
            </ul>
        })
        return (
            <div>
                {userList}
            </div>
        )
    }
}

const mapStateToProps = state => {
    console.log(`msp`)
    console.log(state)
    return {
        fetch: state.data.data
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUser: () => dispatch(actions.fetchData())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Data)
